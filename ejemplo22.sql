﻿SET NAMES 'utf8';
USE ejemplo22;

-- COMBINACION NATURAL ENTRE AUTORES Y NOTICIAS
SELECT * 
  FROM noticias n NATURAL JOIN autores a;

-- COMBINACION INTERNA
SELECT * 
  FROM noticias n JOIN autores a
    USING(idAutor);

SELECT * 
  FROM noticias n JOIN autores a
    ON n.idAutor = a.idAutor;



  -- INTERSECCION
  -- LISTAR LOS NOMBRES DE LOS AUTORES QUE COMIENZAN CON LA
  -- MISMA LETRA QUE TERMINE ALGUN TITULO DE UNA NOTICIA

    -- c1
    SELECT LEFT(a.nombre,1) AS caracter
      FROM autores a;

    -- c2
    SELECT RIGHT(n.titulo,1) AS caracter
      FROM noticias n;

    -- interseccion
    SELECT DISTINCT c1.caracter FROM 
      (
        SELECT LEFT(a.nombre,1) AS caracter
          FROM autores a
      ) AS c1
      NATURAL JOIN
      (
        SELECT RIGHT(n.titulo,1) AS caracter
          FROM noticias n
      ) AS c2;

    -- INTERSECCION CON JOIN
      SELECT DISTINCT c1.caracter FROM 
        (
          SELECT LEFT(a.nombre,1) AS caracter
            FROM autores a
        ) AS c1
      JOIN
        (
          SELECT RIGHT(n.titulo,1) AS caracter
            FROM noticias n
        ) AS c2
       USING(caracter);

      -- terminarla

      SELECT * FROM autores a
        WHERE LEFT(a.nombre,1) IN 
          (
            SELECT DISTINCT c1.caracter FROM 
              (
                SELECT LEFT(a.nombre,1) AS caracter
                  FROM autores a
              ) AS c1
            JOIN
              (
                SELECT RIGHT(n.titulo,1) AS caracter
                  FROM noticias n
              ) AS c2
             USING(caracter)
          );

      -- realizarlo toda la consulta con un
      -- inner join

    SELECT DISTINCT a.* 
      FROM autores a JOIN noticias n
      ON LEFT(a.nombre,1)=RIGHT(n.titulo,1);


    -- STRAIGHT JOIN
    -- SIN ON

      SELECT * 
        FROM autores a STRAIGHT_JOIN  noticias n;
            
      -- LO MISMO QUE CON JOIN
      SELECT * 
        FROM autores a JOIN  noticias n;

    -- STRAIGHT JOIN
    -- CON ON

      SELECT * 
        FROM autores a STRAIGHT_JOIN  noticias n
        ON a.idAutor = n.idAutor;
            
      -- LO MISMO QUE CON JOIN
      SELECT * 
        FROM autores a JOIN  noticias n
        ON a.idAutor = n.idAutor;
  
    -- STRAIGHT JOIN
    -- CON USING 
    -- NO FUNCIONA

      SELECT * 
        FROM autores a STRAIGHT_JOIN  noticias n
        USING(idAutor); 
      
        
    -- CROSS JOIN 
    -- producto cartesiano
      
      SELECT * FROM noticias n CROSS JOIN autores a;     
      
      -- realizado con el join
      SELECT * FROM noticias n JOIN autores a;     
      
      -- realizado con la ","
      SELECT * FROM noticias n,autores a;     

  -- CROSS JOIN   
  -- EN MYSQL NOS PERMITE COLOCAR ON, USIGN 

    SELECT * 
      FROM noticias n CROSS JOIN autores a
      ON n.idAutor = a.idAutor;
    
    SELECT * 
      FROM noticias n CROSS JOIN autores a
      USING(idAutor);

-- ejemplos de aplicacion del natural join
-- el natural join se utiliza 
-- combinar dos tablas por los campos que se llaman igual

-- esto nos permite utilizarle para la interseccion

-- NOTICIAS CUYO TITULO COINCIDE CON EL NOMBRE
-- DE ALGUN AUTOR

-- combinacion interna
  SELECT * 
    FROM noticias n JOIN autores a 
    ON n.titulo=a.nombre;

-- si quiero realizar esto mediante una 
-- combinacion natural

-- c1
SELECT n.idNoticia,
       n.titulo AS nombre,
       n.idAutor AS autor
  FROM noticias n;

-- solucion
  SELECT * 
    FROM autores a NATURAL JOIN 
    (
      SELECT n.idNoticia,
         n.titulo AS nombre,
        n.idAutor AS autor
      FROM noticias n
    ) AS c1;