﻿SET NAMES 'utf8';
USE ciclistas;

-- ejemplo de natural join
-- El dorsal del ciclista que ha ganado puertos y etapas

-- al realizarlo directamente 
-- cometo errores
SELECT DISTINCT e.dorsal
  FROM puerto p NATURAL JOIN etapa e;

-- realizandola por partes

-- c1
-- ciclistas que han ganado puertos
SELECT DISTINCT p.dorsal FROM puerto p;

-- c2
-- ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- termino la consulta con
-- natural join

SELECT * 
  FROM 
    (SELECT DISTINCT p.dorsal FROM puerto p) c1
  NATURAL JOIN 
    (SELECT DISTINCT e.dorsal FROM etapa e) c2;

SELECT * 
  FROM 
    (SELECT DISTINCT p.dorsal FROM puerto p) c1
  JOIN 
    (SELECT DISTINCT e.dorsal FROM etapa e) c2
  USING(dorsal);

-- esta consulta en otro sistema gestor de base de datos
-- se realizaria con una INTERSECT 

SELECT DISTINCT p.dorsal FROM puerto p
  INTERSECT   
SELECT DISTINCT e.dorsal FROM etapa e;


-- SI EN LA CONSULTA ANTERIOR ME PIDIESEN TODOS
-- LOS DATOS DE LOS CICLISTAS
SELECT * FROM 
  (
    SELECT * 
      FROM 
        (SELECT DISTINCT p.dorsal FROM puerto p) c1
      NATURAL JOIN 
        (SELECT DISTINCT e.dorsal FROM etapa e) c2
  ) c3
  JOIN ciclista c
  USING (dorsal);
